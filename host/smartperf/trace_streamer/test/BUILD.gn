# Copyright (C) 2021 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("//build/test.gni")
import("test_ts.gni")

if (target == "test") {
  ohos_unittest("trace_streamer_ut") {
    sources = [
      "unittest/binder_filter_test.cpp",
      "unittest/bytrace_parser_test.cpp",
      "unittest/clock_filter_test.cpp",
      "unittest/cpu_filter_test.cpp",
      "unittest/event_parser_test.cpp",
      "unittest/filter_filter_test.cpp",
      "unittest/hidump_parser_test.cpp",
      "unittest/hilog_parser_test.cpp",
      "unittest/htrace_binder_event_test.cpp",
      "unittest/htrace_diskio_parser_test.cpp",
      "unittest/htrace_event_parser_test.cpp",
      "unittest/htrace_irq_event_test.cpp",
      "unittest/htrace_mem_parser_test.cpp",
      "unittest/htrace_network_parser_test.cpp",
      "unittest/htrace_process_parser_test.cpp",
      "unittest/htrace_sys_mem_parser_test.cpp",
      "unittest/htrace_sys_vmem_parser_test.cpp",
      "unittest/http_server_test.cpp",
      "unittest/irq_filter_test.cpp",
      "unittest/measure_filter_test.cpp",
      "unittest/native_hook_parser_test.cpp",
      "unittest/parser_test.cpp",
      "unittest/process_filter_test.cpp",
      "unittest/rpc_server_test.cpp",
      "unittest/slice_filter_test.cpp",
      "unittest/wasm_func_test.cpp",
    ]
    deps = [
      "../src:trace_streamer_source",
      "//prebuilts/protos:ts_proto_data_cpp",
      "//third_party/googletest:gtest",
      "//third_party/googletest:gtest_main",
      "//third_party/protobuf:protobuf",
      "//third_party/protobuf:protobuf_lite",
      "//third_party/sqlite:sqlite",
    ]
    include_dirs = [
      "../src",
      "../src/trace_data",
      "../src/table",
      "../src/filter",
      "../src/base",
      "../src/rpc",
      "../src/include",
      "../src/trace_streamer",
      "../src/parser/bytrace_parser",
      "../src/parser",
      "../src/cfg",
      "../src/parser/htrace_parser",
      "../src/parser/htrace_parser/htrace_event_parser",
      "../src/parser/htrace_parser/htrace_cpu_parser",
      "../prebuilts/emsdk/emsdk/emscripten/system/include",
      "..",
      "//third_party/googletest/googletest/include/gtest",
      "//utils/native/base/include",
      "//third_party/protobuf/src",
      "${OHOS_PROTO_GEN}",
      "${OHOS_FTRACE_PROTO_DIR}",
      "${OHOS_MEMORY_PROTO_DIR}",
      "${OHOS_HILOG_PROTO_DIR}",
      "${OHOS_NATIVE_HOOK_PROTO_DIR}",
      "${OHOS_HIDUMP_PROTO_DIR}",
      "${OHOS_NETWORK_PROTO_DIR}",
      "${OHOS_DISKIO_PROTO_DIR}",
      "${OHOS_CPUDATA_PROTO_DIR}",
      "${OHOS_PROCESS_PROTO_DIR}",
      "${OHOS_SERVICE_PROTO_DIR}",
    ]
    cflags = [
      "-Wno-inconsistent-missing-override",
      "-Dprivate=public",  #allow test code access private members
      "-fprofile-arcs",
      "-ftest-coverage",
      "-Wno-unused-command-line-argument",
      "-Wno-format",
      "-Wno-unused-const-variable",
      "-Wno-unused-variable",
      "-Wno-used-but-marked-unused",
    ]
    ldflags = [
      "-fprofile-arcs",
      "-ftest-coverage",
      "--coverage",
    ]
    cflags += [
      # clang coverage options:
      "--coverage",
      "-mllvm",
      "-limited-coverage-experimental=true",
      "-fno-use-cxa-atexit",
    ]
    libs = [ "LLVMCore" ]
  }
}

# this is the dest for ohos.build
if (target == "test") {
  group("unittest") {
    testonly = true
    deps = [ ":trace_streamer_ut" ]
  }
} else if (target == "fuzz") {
  group("fuzztest") {
    testonly = true
    deps = [
      "test_fuzzer/bytrace_fuzzer:fuzztest",
      "test_fuzzer/htrace_fuzzer:fuzztest",
      "test_fuzzer/selector_fuzzer:fuzztest",
    ]
  }
}
